#include <stdio.h>
#include <stdlib.h>

#define N 100000

typedef struct player {
    int efficiency;
    int number;
} player;

int cmp_efficiency(const void *player1, const void *player2) {
    return ((player*) player1)->efficiency - ((player*) player2)->efficiency;
}

int cmp_number(const void *player1, const void *player2) {
    return ((player*) player1)->number - ((player*) player2)->number;
}

int main() {
    int n;
    scanf("%d", &n);

    player *players = malloc(sizeof(player) * n);

    for (int i = 0; i < n; i++) {
        int efficiency;
        scanf("%d", &efficiency);

        players[i].number = i + 1;
        players[i].efficiency = efficiency;
    }

    if (n == 1) {
        printf("%d\n", players[0].efficiency);
        printf("%d\n", players[0].number);
        return 0;
    }

    qsort(players, n, sizeof(player), cmp_efficiency);

    int weakest = 0;
    long long max_team_efficiency = ((unsigned int) players[weakest].efficiency) + ((unsigned int) players[weakest + 1].efficiency);
    long long team_efficiency = max_team_efficiency;
    int min_player = 0, max_player = n - 1;

    for (int i = 2; i < n; i++) {
        long long weakest_efficiencies = ((unsigned int) players[weakest].efficiency) + ((unsigned int) players[weakest + 1].efficiency);
        int efficiency = players[i].efficiency;

        while (weakest_efficiencies < efficiency && weakest < i - 2) {
            weakest_efficiencies = weakest_efficiencies - ((unsigned int) players[weakest].efficiency) + ((unsigned int) players[weakest + 2].efficiency);
            team_efficiency -= (unsigned int) players[weakest].efficiency;
            weakest++;
        }

        team_efficiency += players[i].efficiency;
        if (weakest_efficiencies >= efficiency && max_team_efficiency < team_efficiency) {
            max_team_efficiency = team_efficiency;
            min_player = weakest;
            max_player = i;
        }
    }

    team_efficiency = ((unsigned int) players[n - 2].efficiency) + ((unsigned int) players[n - 1].efficiency);
    if (team_efficiency > max_team_efficiency) {
        max_team_efficiency = team_efficiency;
        min_player = n - 2;
        max_player = n - 1;
    }

    qsort(players + min_player, max_player - min_player + 1, sizeof(player), cmp_number);

    printf("%llu\n", max_team_efficiency);
    for (int i = min_player; i <= max_player; i++) {
        printf("%d ", players[i].number);
    }
    printf("\n");

    free(players);

    return 0;
}
